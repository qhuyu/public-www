# Privacy policy
1. Use of information
The company will not provide, sell, lease, share or trade your personal information to any irrelevant third party, unless you have obtained your permission in advance, or the third party and the company (including the company's affiliates) provide services for you alone or jointly, and after the end of the service, they will be prohibited from accessing, including all these materials they can access before.
b) The company also does not allow any third party to collect, edit, sell or disseminate your personal information free of charge by any means. If any platform user of the company engages in the above activities, Suzhou Lezhi has the right to immediately terminate the service agreement with the user.

2. Information storage and exchange
The information and materials about you collected by the company will be saved on the server of the company on the corresponding platform. These information and materials may be transmitted to your country, region or overseas where the company collects information and materials, and accessed, stored and displayed overseas.
The authority required by the company is as follows:
Allow programs to access information about GSM networks
  allow programs to open network sockets
  external storage read and write permission
The above permissions are only used for the most basic game services and will not obtain other permissions including privacy.

3. Your rights
In accordance with the relevant laws, regulations and standards of China and the common practices of other countries and regions, we guarantee that you exercise the following rights to your user information:
a. Access your user information
  you have the right to access your user information, except for exceptions provided by laws and regulations. If you want to exercise the data access right, you can access it by yourself in the following ways: platform account function access.
b. Correct your user information
  when you find that there are errors in the user information we handle about you, you have the right to ask us to make corrections. You can apply for correction by accessing the methods listed in your user information.
c. Delete your user information
you can request us to delete user information under the following circumstances:
		1. If our behavior of processing user information violates laws and regulations;
		2. If we collect and use your user information without your consent;
		3. If our handling of user information violates the agreement with you;
		4. If you no longer use our products or services, or you cancel your account;
		5. If we no longer provide products or services for you.  

we will evaluate your deletion request. If the corresponding provisions are met, we will take corresponding steps to deal with it. When you make a deletion request to us, we may require you to authenticate to ensure the security of your account. After you delete information from our service, due to applicable laws and security technologies, we may not immediately delete the corresponding information from the backup system. We will safely store your information until the backup can be cleared or anonymous.
or anonymize your information according to your requirements, unless otherwise stipulated by laws and regulations. This may also cause you to lose access to the data in your account. Please be careful.

  we strictly abide by the provisions of laws and regulations and the agreement with users, and use the collected information for the following purposes. If we use your information beyond the following purposes, we will explain it to you again and obtain your consent.
  1. Meet your personalized needs. For example, language settings and personalized help services
  2. Product development and service optimization, for example, when our system fails, we will record and analyze the information generated when the system fails to optimize our services.
  3. Evaluate and improve the effectiveness of our promotional and promotional activities

Revision of privacy policy:

This game will often modify the privacy policy. If there is any change in the user's personal information policy, we will notify the user in time.
Problems and suggestions:
If you have other questions and suggestions, please let us know.

tsangpozheng@gmail.com